// Copyright (c) 2022 Yuri6037
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import SwiftUI

func getFlagsString(flags: bpx_u8_t) -> String {
    var flagshj = String();
    if flags & bpx_u8_t(BPX_FLAG_COMPRESS_ZLIB) == bpx_u8_t(BPX_FLAG_COMPRESS_ZLIB) {
        flagshj += " | CompressZlib";
    }
    if flags & bpx_u8_t(BPX_FLAG_COMPRESS_XZ) == bpx_u8_t(BPX_FLAG_COMPRESS_XZ) {
        flagshj += " | CompressXZ";
    }
    if flags & bpx_u8_t(BPX_FLAG_CHECK_CRC32) == bpx_u8_t(BPX_FLAG_CHECK_CRC32) {
        flagshj += " | CheckCrc32";
    }
    if flags & bpx_u8_t(BPX_FLAG_CHECK_WEAK) == bpx_u8_t(BPX_FLAG_CHECK_WEAK) {
        flagshj += " | CheckWeak";
    }
    if flags & bpx_u8_t(BPX_FLAG_CHECK_WEAK) != bpx_u8_t(BPX_FLAG_CHECK_WEAK)
        && flags & bpx_u8_t(BPX_FLAG_CHECK_CRC32) != bpx_u8_t(BPX_FLAG_CHECK_CRC32)
    {
        flagshj += " | CheckNone";
    }
    //Potentially super slow code but no other options despite my string to obviously be always ASCII characters, swift refuses at all costs to use an int directly! Let it be a good old Theta(n).
    return String(flagshj[flagshj.index(flagshj.startIndex, offsetBy: 2)...]);
}

@ViewBuilder func renderSectionInfo(header: bpx_section_header_t) -> some View {
    VStack {
        HStack {
            Text("Pointer: ")
                .fontWeight(Font.Weight.bold)
            Spacer()
            Text(String(format: "0x%X", header.pointer))
        }
        HStack {
            Text("Compressed size: ")
                .fontWeight(Font.Weight.bold)
            Spacer()
            Text(String(header.csize) + " byte(s)")
        }
        HStack {
            Text("Size: ")
                .fontWeight(Font.Weight.bold)
            Spacer()
            Text(String(header.size) + " byte(s)")
        }
        HStack {
            Text("Checksum: ")
                .fontWeight(Font.Weight.bold)
            Spacer()
            Text(String(header.chksum))
        }
        HStack {
            Text("Type: ")
                .fontWeight(Font.Weight.bold)
            Spacer()
            Text(String(format: "0x%X", header.ty))
        }
        HStack {
            Text("Flags: ")
                .fontWeight(Font.Weight.bold)
            Spacer()
            Text(getFlagsString(flags: header.flags))
        }
    }.font(.system(size: 20.0))
}

func get_ascii_char(byte: UInt8) -> String {
    if byte < 0x20 || byte == 0x7F {
        return ".";
    } else {
        return String(bytes: [byte], encoding: .ascii)!;
    }
}

func getRenderedText(buffer: [bpx_u8_t]) -> String
{
    let max = 8;
    var str = String(format: "%6X  ", 0);
    var ascii = "";
    for i in 0..<buffer.count {
        let s = String(format: "%02X", buffer[i]);
        ascii += get_ascii_char(byte: buffer[i]);
        str += s + " ";
        if (i + 1) % max == 0 {
            str += " " + ascii + "\n" + String(format: "%6X  ", i);
            ascii = "";
        }
    }
    for _ in 0...(max - ascii.count - 1) {
        str += "   ";
    }
    str += " " + ascii;
    return str;
}

@ViewBuilder func renderSectionData(buffer: [bpx_u8_t]) -> some View {
    ScrollView {
        Text(getRenderedText(buffer: buffer))
            .padding()
            .font(.system(size: 12, weight: .regular, design: .monospaced))
            .frame(maxWidth: .infinity)
    }.frame(maxWidth: .infinity)
}

struct ContentView: View {
    @Binding var document: BPXgui2Document
    @State var selected: UInt32 = 0

    var body: some View {
        if let err = document.error {
            VStack {
                Text(String(format: "BPX Error %d", err.code)).padding()
                Text(err.description)
                Text("Check the file: it might have been corrupted/truncated!")
            }
        } else {
            TabView {
                ScrollView {
                    ForEach(document.container!.sections, id: \.index) {section in
                        HStack {
                            if selected == section.index {
                                Rectangle().fill(Color.accentColor).frame(width: 2.0)
                                    .fixedSize(horizontal: false, vertical: false)
                            } else {
                                Rectangle().fill(Color.white).frame(width: 2.0)
                                    .fixedSize(horizontal: false, vertical: false)
                            }
                            Text(section.name) //This part is a hack due to SwiftUI inability to loop over elements of an arbitary list
                                .font(.system(size: 30.0))
                            if selected != section.index {
                                Button(action: {selected = section.index}) {
                                    Text("Select")
                                }
                            }
                            Spacer()
                        }.padding(7.0)
                            .background(Color(.sRGB, red: 0.969, green: 0.969, blue: 0.969))
                            .cornerRadius(8)
                    }
                }.padding().tabItem {
                        Image(systemName: "phone.fill")
                        Text("Section List")
                    }
                renderSectionInfo(header: document.container!.sections[Int(selected)].header)
                    .padding()
                    .tabItem {
                        Image(systemName: "phone.fill")
                        Text("Section Info")
                    }
                renderSectionData(buffer: document.container!.sections[Int(selected)].data)
                    .padding()
                    .tabItem {
                        Image(systemName: "phone.fill")
                        Text("Section Data")
                    }
            }
        }
    }
}
