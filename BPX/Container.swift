// Copyright (c) 2022 Yuri6037
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import Foundation

private func getSectionData(_ container: OpaquePointer, handle: bpx_handle_t, header: bpx_section_header_t) throws -> [bpx_u8_t] {
    let err = bpx_section_seek(container, handle, 0);
    if err != BPX_ERR_NONE {
        throw BpxcError(fromRaw: err);
    }
    var data: [bpx_u8_t] = [bpx_u8_t](repeating: 0, count: Int(header.size));
    let err1 = data.withUnsafeMutableBufferPointer { ptr in bpx_section_read(container, handle, ptr.baseAddress!, Int(header.size)) };
    if err1 != BPX_ERR_NONE {
        throw BpxcError(fromRaw: err)
    }
    return data;
}

private func getSectionHeader(_ container: OpaquePointer, handle: bpx_handle_t) -> bpx_section_header_t {
    var header = bpx_section_header_s();
    bpx_section_get_header(container, handle, &header);
    return header;
}

private func loadContainer(_ container: OpaquePointer) throws -> (bpx_main_header_t, [Section]) {
    var header: bpx_main_header_t = bpx_main_header_s();
    bpx_container_get_main_header(container, &header);
    var rows: [Section] = [];
    rows.reserveCapacity(Int(header.section_num));
    var arr: [bpx_handle_t] = [bpx_handle_t](repeating: 0, count: Int(header.section_num));
    arr.withUnsafeMutableBufferPointer { ptr in
        bpx_container_list_sections(container, ptr.baseAddress!, Int(header.section_num));
    };
    for (i, h) in arr.enumerated() {
        let header = getSectionHeader(container, handle: h);
        let data = try getSectionData(container, handle: h, header: header);
        let name = String(format:"Section #%d", i);
        rows.append(Section(name: name, header: header, data: data, index: bpx_u32_t(i)));
    }
    return (header, rows);
}

public class Container {
    public let header: bpx_main_header_t;
    public let sections: [Section];

    public init(path: String) throws {
        var container: OpaquePointer?;
        let err = bpx_container_open(path, &container);
        defer {
            if container != nil {
                bpx_container_close(&container);
            }
        }
        if err != BPX_ERR_NONE {
            throw BpxcError(fromRaw: err);
        }
        (self.header, self.sections) = try loadContainer(container!);
    }

    public init(file: FileWrapper) throws {
        guard let data = file.regularFileContents else {
            throw CocoaError(.fileReadCorruptFile)
        };
        let file = ContainerIo(file: data);
        var io = bpx_container_io_s();
        io.userdata = UnsafeRawPointer(Unmanaged.passUnretained(file).toOpaque());
        io.read = lowLevelRead;
        io.seek = lowLevelSeek;
        var container: OpaquePointer?;
        let err = bpx_container_open2(io, &container);
        defer {
            if container != nil {
                bpx_container_close(&container);
            }
        }
        if err != BPX_ERR_NONE {
            throw BpxcError(fromRaw: err);
        }
        (self.header, self.sections) = try loadContainer(container!);
    }
}
