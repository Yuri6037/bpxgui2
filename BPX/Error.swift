// Copyright (c) 2022 Yuri6037
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import Foundation

public enum CoreError: Error {
    case badChecksum
    case io
    case badVersion
    case badSignature
    case inflate
    case deflate
    case sectionNotLoaded
    case capacity
}

public struct BpxcError: Error {
    public enum Kind {
        case invalidPath
        case fileOpen
        case fileCreate
        case sectionIo
        case unknown
        case core(inner: CoreError)
    }

    public let code: bpx_error_t;
    public let kind: Kind;
    public let description: String;

    public init(fromRaw code: bpx_error_t) {
        self.code = code;
        switch Int32(code) {
        case BPX_ERR_INVALID_PATH:
            self.kind = .invalidPath;
            self.description = "Invalid path";
        case BPX_ERR_FILE_OPEN:
            self.kind = .fileOpen;
            self.description = "Failed to open file";
        case BPX_ERR_FILE_CREATE:
            self.kind = .fileCreate;
            self.description = "Failed to create file";
        case BPX_ERR_SECTION_IO:
            self.kind = .sectionIo;
            self.description = "Low-level io error in section";
        case BPX_ERR_CORE_CHKSUM:
            self.kind = .core(inner: CoreError.badChecksum);
            self.description = "Invalid checksum";
        case BPX_ERR_CORE_IO:
            self.kind = .core(inner: CoreError.io);
            self.description = "Low-level io error";
        case BPX_ERR_CORE_BAD_VERSION:
            self.kind = .core(inner: CoreError.badVersion);
            self.description = "Bad version";
        case BPX_ERR_CORE_BAD_SIGNATURE:
            self.kind = .core(inner: CoreError.badSignature);
            self.description = "Bad signature / not a BPX container";
        case BPX_ERR_CORE_INFLATE:
            self.kind = .core(inner: CoreError.inflate);
            self.description = "Inflate error";
        case BPX_ERR_CORE_DEFLATE:
            self.kind = .core(inner: CoreError.deflate);
            self.description = "Deflate error";
        case BPX_ERR_CORE_SECTION_NOT_LOADED:
            self.kind = .core(inner: CoreError.sectionNotLoaded);
            self.description = "Section not loaded";
        case BPX_ERR_CORE_CAPACITY:
            self.kind = .core(inner: CoreError.capacity);
            self.description = "Capacity exceeded";
        default:
            self.kind = .unknown;
            self.description = "Unknown error";
        }
    }
}
