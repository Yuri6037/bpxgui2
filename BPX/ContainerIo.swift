// Copyright (c) 2022 Yuri6037
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import Foundation

class ContainerIo
{
    private let file: Data;
    private var cursor: Int;

    init(file: Data) {
        self.file = file;
        self.cursor = 0;
    }
    
    func read(buffer: UnsafeMutablePointer<bpx_u8_t>, size: Int, bytes_read: UnsafeMutablePointer<Int>)
    {
        let actual = min(size, self.file.count - self.cursor);
        if actual == 0 {
            bytes_read.pointee = 0;
            return;
        }
        self.file.withUnsafeBytes({ ptr in
            let memory = UnsafeMutableBufferPointer(start: buffer, count: actual);
            ptr[self.cursor...].copyBytes(to: memory);
        });
        bytes_read.pointee = actual;
        self.cursor += actual;
    }

    func seek(from: bpx_seek_from_t, pos: bpx_u64_t, new_pos: UnsafeMutablePointer<bpx_u64_t>) {
        switch from {
        case BPX_SEEK_START:
            self.cursor = Int(pos);
            break;
        default: //Both BPX_SEEK_CURRENT and BPX_SEEK_END do the same
            self.cursor += Int(pos);
            break;
        }
        new_pos.pointee = bpx_u64_t(self.cursor);
    }
}

func lowLevelRead(userdata: UnsafeRawPointer?, buffer: UnsafeMutablePointer<bpx_u8_t>?, size: Int, bytes_read: UnsafeMutablePointer<Int>?) -> bpx_error_t {
    let io: ContainerIo = Unmanaged.fromOpaque(userdata!).takeUnretainedValue(); //Unwrap wrapper
    io.read(buffer: buffer!, size: size, bytes_read: bytes_read!);
    return bpx_error_t(BPX_ERR_NONE);
}

func lowLevelSeek(userdata: UnsafeRawPointer?, from: bpx_seek_from_t, pos: bpx_u64_t, new_pos: UnsafeMutablePointer<bpx_u64_t>?) -> bpx_error_t {
    let io: ContainerIo = Unmanaged.fromOpaque(userdata!).takeUnretainedValue(); //Unwrap wrapper
    io.seek(from: from, pos: pos, new_pos: new_pos!);
    return bpx_error_t(BPX_ERR_NONE);
}
